import time
import orjson
import math
import scrapy
from scrapy import signals
from urllib.parse import urlencode
from scrapy.http import JsonRequest
from spiders.items import GrpItem
from selenium import webdriver
import undetected_chromedriver as uc


class TstnSpider(scrapy.Spider):
    name = "tstn"
    allowed_domains = ["tstn.ru"]
    mongo_collection = "tstn"
    start_url = 'https://tstn.ru/shop/'

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal = signals.spider_closed)
        crawler.signals.connect(spider.spider_opened, signal = signals.spider_opened)
        return spider

    def spider_opened(self, spider):
        options = webdriver.ChromeOptions()
        options.headless = False
        self.driver = uc.Chrome(options=options)
        self.driver.get(self.start_url)
        time.sleep(10)
        self.cookies = {
            cookie['name']: cookie['value']
            for cookie in self.driver.get_cookies()
        }

    def spider_closed(self, spider):
        self.driver.close()

    custom_settings = {
        # 'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 10,
        'DOWNLOAD_DELAY': 0.3,
        #'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,

        # #'ITEM_PIPELINES': {
        #     'spiders.pipelines.MongoPipeline': 300,
        # }
    }

    def start_requests(self):
        yield scrapy.Request(
            self.start_url, cookies = self.cookies, callback = self.catalog
        )

    def catalog(self, response):
        categories = response.xpath('.//li[@class="catalog-category__item"]/a/@href').getall()
        for category in categories:
            yield scrapy.Request(
                url = 'https://www.tstn.ru' + category,
                callback = self.parse_category
            )

    def parse_category(self, response):
        products = response.xpath(".//div[@class='b-product__wrap']/a/@href")
        print('---', len(products))
        # for product in products:
        #     yield response.follow(product.get(), self.item_parse)

        next_page = response.xpath('//a[contains(@class, "pagination__nav-item--next")]/@href').get()
        if next_page:
            yield response.follow(next_page, self.parse_category)


