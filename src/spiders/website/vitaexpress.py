import time
import scrapy
from help.tracking import tracking_request, tracking_response
from scrapy_playwright.page import PageMethod
import undetected_chromedriver as uc

async def init_page(page, request):
    # page.on("request", tracking_request)
    # page.on("response", tracking_response)
    url = "https://vitaexpress.ru/catalog/"
    async with page.expect_response(lambda response: response.url == url and response.status == 200, timeout = 30 * 1000):
        time.sleep(10)


async def scroll_page(page, request):

    for _ in range(0, 3):
        await page.evaluate('''() => {window.scrollTo(0, document.body.scrollHeight)}''')


def should_abort_request(request):
    return (
        request.resource_type == "image"
        or ".jpg" in request.url
    )


from shutil import which
from scrapy_selenium import SeleniumRequest
from selenium import webdriver
# Create a request interceptor

def interceptor(request):
    del request.headers['Referer']  # Delete the header first
    request.headers['Referer'] = 'some_referer'

class VitaexpressSpider(scrapy.Spider):
    name = "vitaexpress"
    allowed_domains = ["vitaexpress.ru"]

    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'SELENIUM_DRIVER_NAME': 'chrome',
        'SELENIUM_DRIVER_EXECUTABLE_PATH': which('chromedriver')
    }

    def __init__(self, driver):
        self.driver = driver

    @classmethod
    def from_crawler(cls, crawler):
        """Initialize the middleware with the crawler settings"""
        options = webdriver.ChromeOptions()
        options.headless = False
        driver = uc.Chrome(options=options)
        return cls(driver = driver)


    def start_requests(self):
        self.driver.get('https://vitaexpress.ru/catalog/')
        self.driver.request_interceptor = interceptor

        # cookies = {
        #     cookie['name']: cookie['value']
        #     for cookie in self.driver.get_cookies()
        # }
        # yield SeleniumRequest(
        #     url='https://vitaexpress.ru/catalog/', cookies=cookies, callback=self.parse, wait_time = 10
        # )

    # def parse(self, response):
    #     print(response.text)

            #'SELENIUM_DRIVER_ARGUMENTS': ['--headless'],

    #     'DOWNLOADER_MIDDLEWARES': {
    #         'scrapy_selenium.SeleniumMiddleware': 800
    #     },
    #
    # # 'DOWNLOAD_HANDLERS': {
    #     #     "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
    #     #     "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
    #     # },
    #     'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
    #     #'PLAYWRIGHT_BROWSER_TYPE': "firefox",
    #     'DOWNLOAD_DELAY': 5,
    #     # 'LOG_LEVEL': 'INFO',
    #     'LOGSTATS_INTERVAL': 5.0,
    #     'CLOSESPIDER_TIMEOUT': 5000,
    #     # "PLAYWRIGHT_MAX_PAGES_PER_CONTEXT": 1,
    #     # 'PLAYWRIGHT_ABORT_REQUEST': should_abort_request,
    #     # "PLAYWRIGHT_LAUNCH_OPTIONS": {
    #     #     "headless": False,
    #     #     "slow_mo": 5000
    #     #     # 'proxy': {
    #     #     #     "server": "http://@proxy.soax.com:9000",
    #     #     #     "username": "FuZxst7BQAxwhDzG",
    #     #     #     "password": "wifi;ru"
    #     #     # }
    #     # }
    # }
    # def start_requests(self):
    #     url = 'https://vitaexpress.ru/catalog/'
    #     yield SeleniumRequest(url=url, callback=self.parse, wait_time=10)
    #
    # def parse(self, response):
    #     print(response.text)
    #
    #     # quote_item = QuoteItem()
    #     # for quote in response.css('div.quote'):
    #     #     quote_item['text'] = quote.css('span.text::text').get()
    #     #     quote_item['author'] = quote.css('small.author::text').get()
    #     #     quote_item['tags'] = quote.css('div.tags a.tag::text').getall()
    #     #     yield quote_item
    #
    # # def start_requests(self):
    # #
    # #     url = 'https://vitaexpress.ru/catalog/'
    # #     yield SeleniumRequest(url=url, callback=self.parse)
    #     # yield scrapy.Request(
    #     #     url = "https://vitaexpress.ru/catalog/",
    #     #     meta = {
    #     #         "playwright": True,
    #     #         "playwright_page_init_callback": init_page,
    #     #         "playwright_include_page": True,
    #     #     },
    #     #     callback = self.category
    #     # )
    #
    # # async def category(self, response):
    # #     page = response.meta["playwright_page"]
    # #
    # #     time.sleep(self.custom_settings.get('DOWNLOAD_DELAY'))
    # #     # await page.locator('a:has-text("Подтверждаю")').click()
    # #     # check_region = page.locator('a:has-text("Подтверждаю")')
    # #
    # #     selector = scrapy.Selector(text=await page.content())
    # #     categories = selector.xpath('.//a[@class="subCat__link"]/@href').getall()
    # #     cookies = {
    # #         cookie['name']: cookie['value']
    # #         for cookie in await page.context.cookies()
    # #     }
    # #     for category in categories:
    # #         yield scrapy.Request(
    # #             url = 'https://vitaexpress.ru' + category,
    # #             cookies = cookies,
    # #             # meta = {
    # #             #     "playwright": True,
    # #             #     "playwright_include_page": page,
    # #             #     "playwright_page_init_callback": scroll_page,
    # #             # },
    # #             callback = self.parse_category
    # #         )
    # #
    # # async def parse_category(self, response):
    # #     #page = response.meta["playwright_page"]
    # #     print('##########', response.url)
    # #
    # #
    # #
    # #
    # # async def errback(self, failure):
    # #     page = failure.request.meta["playwright_page"]
    # #
    # #     for _ in range(0, 3):
    # #         await page.evaluate('''() => {window.scrollTo(0, document.body.scrollHeight)}''')
    # #         time.sleep(5)
    #
    #     # products = response.xpath(".//div[@class='b-product__wrap']/a/@href")
    #     # print(len(products))
    #     #
    #     # # for product in products:
    #     # #     print(product)
    #     # next_page = response.xpath('//a[contains(@class, "pagination__nav-item--next")]/@href').get()
    #     # if next_page:
    #     #     yield response.follow(next_page, self.parse_catego