import time
import orjson
import math
import scrapy
from scrapy import signals
from urllib.parse import urlencode
from scrapy.http import JsonRequest
from spiders.items import GrpItem
from selenium import webdriver
import undetected_chromedriver as uc


class AuchanSpider(scrapy.Spider):
    name = "auchan"
    allowed_domains = ["auchan.ru"]
    mongo_collection = "auchan"

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 2,
        'DOWNLOAD_DELAY': 2,
        'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,

        'ITEM_PIPELINES': {
            'spiders.pipelines.MongoPipeline': 300,
        },

    }

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal = signals.spider_closed)
        crawler.signals.connect(spider.spider_opened, signal = signals.spider_opened)
        return spider

    def spider_opened(self, spider):
        options = webdriver.ChromeOptions()
        options.headless = True
        self.driver = uc.Chrome(
            options = options, version_main = 110
        )
        self.driver.get('https://www.auchan.ru/catalog/')
        time.sleep(10)
        self.cookies = {
            cookie['name']: cookie['value']
            for cookie in self.driver.get_cookies()
        }

    def spider_closed(self, spider):
        self.driver.close()

    def start_requests(self):
        yield scrapy.Request(
            'https://www.auchan.ru/catalog/', cookies = self.cookies, callback = self.catalog
        )

    def catalog(self, response):
        data = response.xpath('//script[@id="init"]/text()').get()
        data = data.replace("window.__INITIAL_STATE__ = ", '')
        data = orjson.loads(data)
        for category in data.get('categories', {}).get('categories', {}):

            code = category.get('code')
            total = category.get('productsCount')
            for num_page in range(1, math.ceil(total / 100) + 1):
                params = {
                    'merchantId': '1',
                    'page': num_page,
                    'perPage': '100',
                }
                data = {
                    'filter': {
                        'category': code,
                        'promo_only': False,
                        'active_only': False,
                        'cashback_only': False
                    }
                }
                url = 'https://www.auchan.ru/v1/catalog/products/?' + urlencode(params)
                yield JsonRequest(
                    url = url, data = data, callback = self.products
                )


    def products(self, response):
        data = orjson.loads(response.text)
        for product in data.get('items'):

            item = GrpItem(
                id = product.pop('id'),
                url = 'https://www.auchan.ru/product/' + product.pop('code'),
                title = product.pop('title'),
                category = [
                    category.get('name')
                    for category in product.get('categoryCodes', None)
                ],
                description = product.get('description', {}).get('content'),
                images = product.pop('mediaUrls'),
                brand = product.get('brand', {}).get('name'),
                vendor_code = product.pop('vendorCode', None),
                vendor = None,
                stock = product.get('stock', {}).get('qty'),
                price = product.get('price', {}).get('value', None),
            )

            price_old = product.get('oldPrice', {})
            if price_old:
                item.price_old = price_old.get('value')

            item.other = product
            yield item
