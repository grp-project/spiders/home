import re
import ast
import time
import scrapy
import undetected_chromedriver as uc
from scrapy import signals
from urllib.parse import urlencode
from selenium import webdriver
from spiders.items import GrpItem
from help.common import celar_html


class EkonikaSpider(scrapy.Spider):

    name = "ekonika"
    allowed_domains = ["ekonika.ru"]
    mongo_collection = "ekonika"

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal = signals.spider_closed)
        crawler.signals.connect(spider.spider_opened, signal = signals.spider_opened)
        return spider

    def spider_opened(self, spider):
        options = webdriver.ChromeOptions()
        options.headless = False
        self.driver = uc.Chrome(options=options)
        self.driver.get('https://ekonika.ru/catalog')
        time.sleep(10)
        self.cookies = {
            cookie['name']: cookie['value']
            for cookie in self.driver.get_cookies()
        }

    def spider_closed(self, spider):
        self.driver.close()

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 5,
        'DOWNLOAD_DELAY': 1,
        'LOG_LEVEL': 'INFO',
        # 'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,

        'ITEM_PIPELINES': {
            'spiders.pipelines.MongoPipeline': 300,
        }
    }

    def start_requests(self):
        url = 'https://ekonika.ru/catalog'
        yield scrapy.Request(
            url = url, cookies = self.cookies, callback = self.catalog
        )

    def catalog(self, response):
        catalog = response.xpath('.//li[@class="catalog-menu__item"]/a/@href').getall()
        categories = [x for x in catalog if 'catalog' in x]
        for category in categories:
            yield response.follow(category, self.parse_category)

    def parse_category(self, response):
        products = response.xpath("//div/@data-id/parent::*")
        for product in products:
            params = {
                'site': 's1',
                'code': product.xpath('./@data-product-id').get(),
                'id': product.xpath('./@data-id').get(),
            }
            yield scrapy.Request(
                url = 'https://ekonika.ru/ajax/product_preview.php/?' + urlencode(params),
                callback = self.detail_parse,
            )
        next_page = response.xpath('//a[contains(@class, "paging-next")]/@href').get()
        if next_page:
            yield response.follow(next_page, self.parse_category)


    def detail_parse(self, response):
        script = response.xpath('//script/text()').get()
        row = celar_html(script)

        layer_first = re.search(r"dataLayer.push\((.*?)\)", row, flags=re.DOTALL)
        layer_second = re.search(r"products': \[(.*?)]", row, flags=re.DOTALL)
        data = dict(
            ast.literal_eval(layer_first.group(1)),
            **ast.literal_eval(layer_second.group(1)),
        )

        properties = response.xpath('//div[@class="fast-view__info"]/dl')

        properties = dict(zip(
            [x.strip() for x in properties.xpath('./dt/span/text()').getall()],
            [x.strip() for x in properties.xpath('./dd/span/text()').getall()]
        ))

        item = GrpItem(
            id = data.get('ProductID'),
            url = response.url,
            sku = response.xpath('.//span[@class="articul-info__articul-text"]/text()').get(),
            title = data.get('name'),
            images = response.xpath('//div[@class="swiper-slide"]/img/@src').getall(),
            category = data.get('category').split('/'),
            price = response.xpath('//div[@class="articul-info__price"]/text()').get(),
            price_old = response.xpath('//div[@class="articul-info__old-price"]/text()').get(),
            brand = data.get('brand'),
            properties = properties
        )
        yield item
