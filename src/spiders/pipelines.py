import pymongo
from itemadapter import ItemAdapter
from core import settings
from spiders.items import GrpItem
from datetime import datetime
from scrapy.exceptions import DropItem

class MongoPipeline:

    collection_name = 'scrapy_items'

    # def __init__(self, mongo_uri, mongo_db):
    #     self.mongo_uri = mongo_uri
    #     self.mongo_db = mongo_db
    #
    # @classmethod
    # def from_crawler(cls, crawler):
    #     return cls(
    #         mongo_uri=crawler.settings.get('MONGO_URI'),
    #         mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
    #     )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(settings.mongo.dns)
        self.db = self.client[settings.mongo.db_name]
        collection = getattr(spider, 'mongo_collection', 'offers')
        self.collection = self.db[collection]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item: GrpItem, spider):
        try:
            self.collection.find_one_and_update(
                filter = {
                    'id': item.id,
                    'url': item.url,
                },
                update = {
                    '$set': {
                        **item.dict(),
                        'store_name': getattr(spider, 'name')
                    },
                    '$push': {
                        'prices': {
                                'value': item.price,
                                'ts': datetime.utcnow(),
                                'stock': item.stock,
                            }
                    }
                },
                upsert=True
            )
            return item
        except (AttributeError, KeyError) as error:
            raise DropItem(f'Mandatory key missed: {error}')

        # self.db[self.collection_name].insert_one(ItemAdapter(item).asdict())
        # return item



# class MongoPipeline:
#
#     # collection_name = 'offers'
#
#     def __init__(self, mongo_uri, mongo_db):
#         self.mongo_uri = mongo_uri
#         self.mongo_db = mongo_db
#         print(mongo_uri, mongo_db)
#
#     @classmethod
#     def from_crawler(cls, crawler):
#         return cls(
#             mongo_uri=crawler.settings.get('MONGO_URI'),
#             mongo_db=crawler.settings.get('MONGO_DATABASE'),
#         )
#
#     def open_spider(self, spider):
#         self.client = pymongo.MongoClient(self.mongo_uri)
#         self.db = self.client[self.mongo_db]
#
#         collection = getattr(spider, 'mongo_collection', 'offers')
#         self.collection = self.db[collection]
#
#     def close_spider(self, spider):
#         self.client.close()
#
#     def process_item(self, item, spider):
#         try:
#             self.collection.find_one_and_update(
#                 filter={
#                     'url': item['url'],
#                     'title': item['title'],
#                 },
#                 update={
#                     '$set': {
#                         # mandatory keys:
#                         'title': item['title'],
#                         'url': item['url'],
#                         'store_name': item.get('store_name', getattr(spider, 'store_name')),
#
#                         # optional keys:
#                         'categories': item.get('categories'),
#                         'sku': item.get('sku'),
#                         'region': item.get('region'),
#                         'vendor': item.get('vendor'),
#                         'description': item.get('description'),
#                         'params': item.get('params', []),
#                         'unit': item.get('unit', 'шт'),
#                         'currency': item.get('currency', 'RUB')
#                     },
#                     '$push': {
#                         'prices': {
#                             'value': item.get('price', .0),
#                             'ts': datetime.datetime.utcnow(),
#                             'in_stock': item.get('in_stock', bool(item.get('price'))),
#                         }
#                     }
#                 },
#                 upsert=True
#             )
#         except (AttributeError, KeyError) as error:
#             raise DropItem(f'Mandatory key missed: {error}')
#         return item