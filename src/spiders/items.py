from pydantic import BaseModel, ValidationError, validator
from typing import List
from pydantic import HttpUrl
from typing import Optional, Any


class GrpItem(BaseModel):
    id: str
    title: str
    url: Optional[HttpUrl] = None
    sku: Optional[str] = None
    category: Optional[List[str]] = []
    description: Optional[str] = None
    properties: Any = None
    images: Optional[List[HttpUrl]] = []
    brand: Optional[str] = None
    rating: Optional[str] = None
    vendor_code: Optional[str] = None
    vendor: Optional[str] = None
    stock: Optional[int] = None
    price: Optional[float] = None
    price_old: Optional[float] = None
    other: Any = None


    # @validator("author")
    # def author_must_start_with_a(cls, value):
    #     if not value.startswith("A") and not value.startswith("a"):
    #         raise ValueError("must starts with 'A' letter")
    #     return value
    #
    # @validator("tags")
    # def only_two_tags_allowed(cls, value):
    #     if len(value) > 2:
    #         raise ValueError("only two tags allowed")
    #     return value