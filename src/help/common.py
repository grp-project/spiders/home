import re


def celar_html(html: str):
    content = re.sub(r"[\n\t\r]", "", html)
    content = re.sub("\s+", ' ', content)
    content = re.sub("\xa0", ' ', content)
    return content