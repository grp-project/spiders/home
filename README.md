# GRP - Spiders - Home

Это дом пауков, они тут живут и работаю. \
Ходят на работу по расписанию или когда их попросят... \
Заставить их работать можно командой

```bash
scarpy crawl <spider_name>
```

Результат их работы это собранные **items** которые они передают в **MongoDB** \
Делать свою работу им помогают разные инструменты:
 - [selenium](https://www.selenium.dev/)
 - [undetected-chromedriver](https://github.com/ultrafunkamsterdam/undetected-chromedriver)
 - [playwright](https://github.com/microsoft/playwright)
 - [scrapy-splash](https://github.com/scrapy-plugins/scrapy-splash)
 - и многое другое можно посмотреть [тут](https://github.com/AccordBox/awesome-scrapy#visual-web-scraping) [тут](https://github.com/lorien/awesome-web-scraping/blob/master/python.md) или [тут](https://github.com/scrapy-plugins)

Пока они только растут и не очень хорошо научились валидировать **items**.


Пауки **full** и **medium** работают по расписанию ~~каждый день~~ раз в неделю они должны отдавать свежую добычу в **MongoDB** и предоставлять отчеты о проделанной работе в **DISCORD** \
Пауки **_detail_** работаю всегда, точнее они дежурят и ничего делают, пока к ним не прилетит задача из очереди **Redis**

За состоянием их здоровья следит **ELK** 

1. Стягиваем свежие изменения и создаем новую ветку.

```bash
git pull origin master
git checkout -b <spider_name>
```
